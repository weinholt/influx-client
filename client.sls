;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2021 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT

;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;; DEALINGS IN THE SOFTWARE.
#!r6rs

;;; Combined UDP client and line protocol formatter for InfluxDB

(library (influx client)
  (export
    open-influx
    close-influx
    influx-send)
  (import
    (rnrs (6))
    (srfi :19)
    (influx socket)
    (influx format))

(define open-influx
  (case-lambda
    (()
     (open-influx "localhost"))
    ((hostname)
     (open-influx hostname "8089"))
    ((hostname port)
     (make-client-socket/udp hostname port))))

(define default-influx #f)

(define (influx-send influx measurement time tags field0 value0 . fields)
  (let ((bv (string->utf8
             (call-with-string-output-port
               (lambda (p)
                 (format-influx-line p measurement time tags
                                     (cons* field0 value0 fields)))))))
    (let ((influx (or influx
                      default-influx
                      (begin
                        (set! default-influx (open-influx))
                        default-influx))))
      (guard (exn ((error? exn) #f))
        (socket-send influx bv)
        #t))))

(define (close-influx x)
  (socket-close x)))
