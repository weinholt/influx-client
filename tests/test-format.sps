#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2021 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT

;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;; DEALINGS IN THE SOFTWARE.
#!r6rs

(import
  (rnrs)
  (srfi :64)
  (influx format))

(test-begin "format")

(define (influx-data->string measurement time tags fields)
  (call-with-string-output-port
    (lambda (p)
      (format-influx-line p measurement time tags fields))))

(test-assert (string=? "measurement a=\"b\"\n"
                       (influx-data->string "measurement" #f '() '(a  "b"))))

(test-assert (string=? "measurement,tag=value a=\"b\"\n"
                       (influx-data->string "measurement" #f '(tag value)
                                            '(a "b"))))

(test-assert (string=? "measurement a=t\n"
                       (influx-data->string "measurement" #f '()
                                            '(a #t))))

(test-assert (string=? "measurement a=f\n"
                       (influx-data->string "measurement" #f '()
                                            '(a #f))))

(test-assert (string=? "measurement a=1234i\n"
                       (influx-data->string "measurement" #f '()
                                            '(a 1234))))

(test-assert (string=? "measurement a=1234u\n"
                       (influx-data->string "measurement" #f '()
                                            '(a (unsigned 1234)))))

(test-assert (string=? "measurement a=18446744073709551615u\n"
                       (influx-data->string "measurement" #f '()
                                            '(a 18446744073709551615))))

(test-assert (string=? "measurement a=-9223372036854775808i\n"
                       (influx-data->string "measurement" #f '()
                                            '(a -9223372036854775808))))

(test-assert (string=? "measurement a=-9223372036854775808i\n"
                       (influx-data->string "measurement" #f '()
                                            '(a -9223372036854775808))))

(test-assert (string=? "measurement a=\"-\\\"-\"\n"
                       (influx-data->string "measurement" #f '()
                                            '(a "-\"-"))))

(test-end)

(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
