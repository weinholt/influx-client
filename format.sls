;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2021 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT

;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;; DEALINGS IN THE SOFTWARE.
#!r6rs

;;; Formats data in the InfluxDB line protocol

;; https://docs.influxdata.com/influxdb/cloud/reference/syntax/line-protocol/

(library (influx format)
  (export
    format-influx-line)
  (import
    (rnrs (6))
    (srfi :19))

(define empty (string->symbol ""))

(define (put-escaped p str escape)
  (do ((i 0 (fx+ i 1)))
      ((fx=? i (string-length str)))
    (let ((c (string-ref str i)))
      (case c
        ;; C-style escapes
        ((#\return) (put-string p "\\r"))
        ((#\page) (put-string p "\\f"))
        ((#\linefeed) (put-string p "\\n"))
        ((#\tab) (put-string p "\\t"))
        (else
         (when (memv c escape)
           (put-char p #\\))
         (put-char p c))))))

;; TODO: Something is wonky with the official description of the
;; escaping. Surely things go wrong if backslashes are not always
;; escaped?

(define put-measurement-name (lambda (p str) (put-escaped p str '(#\, #\space))))
(define put-tag-key (lambda (p sym) (put-escaped p (symbol->string sym) '(#\, #\= #\space))))
(define put-tag-value put-tag-key)
(define put-field-key put-tag-key)
(define put-field-value (lambda (p str) (put-escaped p str '(#\" #\\))))

;; Format a data point in the InfluxDB line protocol. Takes a textual
;; output port and an influx-data record.
(define (format-influx-line p measurement time tags fields)
  ;; Write the measurement name
  (put-measurement-name p measurement)

  ;; Write the tags (optional)
  (do ((tags tags (cddr tags)))
      ((null? tags))
    (when (null? (cdr tags))
      (assertion-violation 'format-influx-line "Invalid tags plist" tags))
    (let* ((key (car tags))
           (value (cadr tags)))
      (put-char p #\,)
      (put-tag-key p key)
      (put-char p #\=)
      (put-tag-value p value)))

  (put-char p #\space)

  ;; Write the fields
  (do ((fields fields (cddr fields)))
      ((null? fields))
    (when (null? (cdr fields))
      (assertion-violation 'format-influx-line "Invalid fields plist" fields))
    (let* ((key (car fields))
           (value (cadr fields)))
      (put-field-key p key)
      (put-char p #\=)
      (cond ((real? value)
             (cond ((inexact? value)
                    ;; This is a bit lazy, but Scheme's number syntax
                    ;; is mostly compatible. An exception is e.g.
                    ;; (expt 2.0 -1074) in Chez Scheme. A cheeky
                    ;; implementation like older Loko Scheme could
                    ;; also do something funny like #i1/2, but lets
                    ;; hope we don't run across such mad lads. Decimal
                    ;; or scientific notation is ok here.
                    (let ((s (number->string value 10)))
                      (let lp ((i 0))
                        (unless (fx=? i (string-length s))
                          (let ((c (string-ref s i)))
                            (unless (eqv? c #\|) ;XXX: denormals
                              (put-char p c)
                              (lp (fx+ i 1))))))))
                   ((integer? value)
                    ;; Either Integer or UInteger
                    (cond ((<= (- (expt 2 63)) value (- (expt 2 63) 1))
                           (display value p)
                           (put-char p #\i))
                          ((<= value (- (expt 2 64) 1))
                           (display value p)
                           (put-char p #\u))
                          (else
                           (assertion-violation 'format-influx-line
                                                "Unrepresentable real" value))))))
            ((boolean? value)
             (put-char p (if value #\t #\f)))
            ((string? value)
             (put-char p #\")
             (put-field-value p value)
             (put-char p #\"))
            ((and (pair? value) (eq? (car value) 'unsigned)
                  (pair? (cdr value))
                  (null? (cddr value)))
             (display (cadr value) p)
             (put-char p #\u))
            (else
             (assertion-violation 'format-influx-line "Unrepresentable type" value)))
      (unless (null? (cddr fields))
        (put-char p #\,))))

  ;; Write the timestamp (optional)
  (when time
    (put-char p #\space)
    (let ((t (time-difference time
                              (date->time-utc
                               (make-date 0 0 0 0 1 1 1970 0)))))
      ;; XXX: If the database uses less precision than
      ;; nanoseconds then this should print an integer in that
      ;; unit, e.g. milliseconds. It's stupid, but that's how
      ;; it works.
      (if (integer? t)
          (display t p)
          (display (+ (time-nanosecond t)
                      (* (expt 10 9) (time-second t)))
                   p))))

  ;; And the terminator
  (put-char p #\newline)))
