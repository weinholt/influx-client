# InfluxDB client for R6RS Scheme

This is intended to help you effortlessly get time series data from
your R6RS Scheme programs into InfluxDB where it can then be
visualized with Grafana.

This package provides a formatter for the InfluxDB line protocol and a
simple UDP client. It can be used together with InfluxDB 1.x and a UDP
listener. (For InfluxDB 2.0 you would apparently need Telegraf; this
is untested).

## Installation

Installing with [Akku.scm](https://akkuscm.org) is simple:

```
akku install influx-client
```

## InfluxDB configuration

Please note that the database must be configured to use nanosecond
precision. If it uses another precision then you will get this obscure
error in the server logs:

```
time outside range -9223372036854775806 - 9223372036854775806
```

## Example

There is a simple no-fuss API if you just want to get started with
inserting data points and you have a reasonably normal InfluxDB setup
with UDP configured on localhost.

```
(import
  (rnrs)
  (influx client))

(influx-send #f "myWallet" #f '(myTag myValue) 'amount 0)
```

## API

### `(import (influx client))`

#### `(open-influx [`*hostname*  *port*`])`

Opens a UDP connection to the given hostname and port and returns an
object representing that connection.

#### `(influx-send` *influx* *measurement* *time* *tags* `.` *fields*`)`

Sends a data point on the given InfluxDB connection.

*influx* may be an object returned by `open-influx`, or `#f`.
The other arguments are as in `format-influx-line`.

This procedure meant to not be blocking, but if *influx* is `#f` then
it needs to look up `localhost` and may stall if DNS is not working
properly, but the connection is only opened for the first call.

More advanced applications will likely want to batch multiple data
point into a single datagram, but that is not possible with this
procedure.

There is no feedback from the server and no way to know if the data
has been sent and received correctly (except by checking the database
yourself).

#### `(close-influx `*x*`)`

Closes the connection that was opened by `open-influx`.

### `(import (influx format))`

#### `(format-influx-line` *p* *measurement* *time* *tags* *fields*`)`

Format a line in the InfluxDB line protocol.

* *p* is a textual output port.
* *measurement* is a string naming the InfluxDB measurement.
* *time* is either a SRFI 19 time-utc object, an exact integer, or
  `#f`. Normally you would use `(current-time time-utc)` from SRFI 19.
  Use `#f` to use the InfluxDB server's time. Use an exact integer to
  write a time in some unit other than nanoseconds, in case the
  database is not configured for nanoseconds.
* *tags*  is a plist of tags, e.g. `()` or `(tag0 value0 tag1 value1)`.
* *fields* is a plist of fields, e.g. `(connections 12 queue 14 latency 30.4)`.
  Values can be strings, booleans, inexact reals, exact 64-bit integers,
  or `(unsigned `*n*`)` to explicitly encode the unsigned integer *n*.
